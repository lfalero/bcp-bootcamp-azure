package com.lfalero.azure.core.service;

import com.lfalero.azure.core.dto.cosmo.StudentDto;

import java.util.List;

public interface StudentService {

    void saveStudent(StudentDto dto);
    void deleteStudent(String id);
    List<StudentDto> getStudents();
    StudentDto getStudent(String id);
}
