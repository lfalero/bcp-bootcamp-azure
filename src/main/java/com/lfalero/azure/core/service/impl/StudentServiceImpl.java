package com.lfalero.azure.core.service.impl;

import com.azure.cosmos.models.PartitionKey;
import com.lfalero.azure.core.dto.cosmo.AddressDto;
import com.lfalero.azure.core.dto.cosmo.StudentDto;
import com.lfalero.azure.core.entity.cosmo.AddressDocument;
import com.lfalero.azure.core.entity.cosmo.StudentDocument;
import com.lfalero.azure.core.exception.StudentNotFoundException;
import com.lfalero.azure.core.repository.cosmo.StudentRepository;
import com.lfalero.azure.core.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    @Transactional
    public void saveStudent(StudentDto dto) {
        StudentDocument studentDocument = new StudentDocument();
        AddressDocument addressDocument = new AddressDocument();

        BeanUtils.copyProperties(dto, studentDocument);

        BeanUtils.copyProperties(dto.getAddress(), addressDocument);
        studentDocument.setAddress(addressDocument);

        studentRepository.save(studentDocument);
    }

    @Override
    @Transactional
    public void deleteStudent(String id) {
        StudentDto studentDto = this.getStudent(id);
        PartitionKey partitionKey = new PartitionKey(studentDto.getLastName());
        studentRepository.deleteById(id, partitionKey);
    }

    @Override
    public List<StudentDto> getStudents() {
        List<StudentDto> studentDtos = new ArrayList<>();

        studentRepository.findAll().forEach(element -> {
            StudentDto studentDto = new StudentDto();

            BeanUtils.copyProperties(element, studentDto);

            if(Objects.nonNull(element.getAddress())) {
                AddressDto addressDto = new AddressDto();
                BeanUtils.copyProperties(element.getAddress(), addressDto);
                studentDto.setAddress(addressDto);
            }
            studentDtos.add(studentDto);
        });
        return studentDtos;
    }

    @Override
    public StudentDto getStudent(String id) {
        StudentDto studentDto = new StudentDto();
        StudentDocument element = studentRepository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));

        BeanUtils.copyProperties(element, studentDto);

        if(Objects.nonNull(element.getAddress())) {
            AddressDto addressDto = new AddressDto();
            BeanUtils.copyProperties(element.getAddress(), addressDto);
            studentDto.setAddress(addressDto);
        }
        return studentDto;
    }
}
