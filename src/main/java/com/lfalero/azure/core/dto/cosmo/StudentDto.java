package com.lfalero.azure.core.dto.cosmo;

import lombok.Data;

@Data
public class StudentDto {

    private String id;
    private String firstName;
    private String lastName;
    private AddressDto address;
}
