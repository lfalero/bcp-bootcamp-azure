package com.lfalero.azure.core.dto.error;

import lombok.Data;

@Data
public class ErrorResponse {

    private String code;
    private String message;
}
