package com.lfalero.azure.core.dto.cosmo;

import lombok.Data;

@Data
public class AddressDto {

    private Integer number;
    private String street;
    private String district;
}
