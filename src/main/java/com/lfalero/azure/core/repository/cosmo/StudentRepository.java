package com.lfalero.azure.core.repository.cosmo;

import com.azure.spring.data.cosmos.repository.CosmosRepository;
import com.lfalero.azure.core.entity.cosmo.StudentDocument;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CosmosRepository<StudentDocument, String> {
}
