package com.lfalero.azure.core.entity.cosmo;

import lombok.Data;

@Data
public class AddressDocument {

    private Integer number;
    private String street;
    private String district;
}
