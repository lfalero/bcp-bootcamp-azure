package com.lfalero.azure.core.entity.cosmo;

import com.azure.spring.data.cosmos.core.mapping.Container;
import com.azure.spring.data.cosmos.core.mapping.GeneratedValue;
import com.azure.spring.data.cosmos.core.mapping.PartitionKey;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
@Container(containerName = "student")
public class StudentDocument {

    @Id
    @GeneratedValue
    private String id;
    private String firstName;

    @PartitionKey
    private String lastName;
    private AddressDocument address;
}
