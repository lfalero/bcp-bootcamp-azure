
package com.lfalero.azure.api;

import com.lfalero.azure.core.dto.cosmo.StudentDto;
import com.lfalero.azure.core.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/student")
@AllArgsConstructor
public class StudentController extends BaseController{

    private final StudentService studentService;

    @PostMapping
    public ResponseEntity saveStudent(@RequestBody StudentDto dto) {
        studentService.saveStudent(dto);
        return buildSuccessResponse();
    }

    @GetMapping
    public ResponseEntity getStudents() {
        List<StudentDto> response = studentService.getStudents();
        return buildSuccessResponse(response);
    }

    @PutMapping
    public ResponseEntity updateStudent(@RequestBody StudentDto dto) {
        studentService.saveStudent(dto);
        return buildSuccessResponse();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteStudent(@PathVariable String id) {
        studentService.deleteStudent(id);
        return buildSuccessResponse();
    }

    @GetMapping("/{id}")
    public ResponseEntity getStudent(@PathVariable String id) {
        StudentDto response = studentService.getStudent(id);
        return buildSuccessResponse(response);
    }
}
