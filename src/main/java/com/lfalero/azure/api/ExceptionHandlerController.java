package com.lfalero.azure.api;

import com.azure.cosmos.implementation.NotFoundException;
import com.lfalero.azure.core.dto.error.ErrorResponse;
import com.lfalero.azure.core.exception.StudentNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(StudentNotFoundException.class)
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public ErrorResponse handleException(StudentNotFoundException exception) {
        ErrorResponse response = new ErrorResponse();
        response.setCode("TL0001");
        response.setMessage(exception.getMessage());
        return response;
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorResponse handleException(NotFoundException exception) {
        ErrorResponse response = new ErrorResponse();
        response.setCode("TL0002");
        response.setMessage(exception.getMessage());
        return response;
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleException(RuntimeException exception) {
        ErrorResponse response = new ErrorResponse();
        response.setCode("TL0003");
        response.setMessage(exception.getMessage());
        return response;
    }

    @ExceptionHandler(NumberFormatException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleException(NumberFormatException exception) {
        ErrorResponse response = new ErrorResponse();
        response.setCode("TL0004");
        response.setMessage(exception.getMessage());
        return response;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleException(Exception exception) {
        ErrorResponse response = new ErrorResponse();
        response.setCode("TL0005");
        response.setMessage(exception.getMessage());
        return response;
    }
}
